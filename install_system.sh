#!/bin/bash

sudo apt-get update
sudo apt-get upgrade


#Private data
while [ "$response" != 'O' ]  &&  [ "$response" != 'N' ];
do
    read -p "Supprimer les logiciels susceptible de violer votre vie privée ? [O/N]" response

    if [ "$response" = "O" ]; then
        sudo apt purge ubuntu-report popularity-contest apport whoopsie
    fi
done



#Chrome
echo 'Installation de chrome'

sudo sh -c 'echo "deb [arch=amd64] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list'
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo apt-get update
sudo apt-get install google-chrome-stable

#Purge of firefox
sudo apt purge firefox

#Libreoffice
echo 'Installation de libreoffice'

sudo apt-get install libreoffice

#Launch new update and remove not used package
sudo apt-get update
sudo apt-get upgrade
sudo apt autoremove


echo 'Installation de base terminée, Reste:
Adblock pour chrome
Teamviwer'

