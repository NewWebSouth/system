echo '
----------------------------------------------
| Format required: username version          |
----------------------------------------------

Your configuration will not be affected
'

echo 'Control parameters...'

if [ -z $1 ]; then
    echo 'Please enter your username in second parameter'
    exit
fi

if [ -z $2 ]; then
    echo 'Please enter your version in third parameter'
    exit
fi


if [ ! -d "/home/$1" ]; then
    echo 'Incorrect username'
    exit
fi

if [ ! -d  "/home/$1/.config/JetBrains/PhpStorm$2" ]; then
    echo "Probably incorrect version, look in directory /home/$1/.config/JetBrains/PhpStorm...,
    Our search: /home/$1/.config/JetBrains/PhpStorm$2"
    exit
fi

echo 'OK'

cd "/home/$1/.config/JetBrains/PhpStorm$2"

if [ -d "eval" ]; then
    rm -r -f eval
fi

cd /home/$1/.java/.userPrefs/jetbrains

if [ -d phpstorm ]; then
    rm -r -f phpstorm
fi

echo '
----------------------------------------------
| Your licence of PhpStorm is reset, enjoy ! |
----------------------------------------------

Please:

- Restart PhpStorm
- Select the evaluation licence
- Look in PhpStorm in Help/About for confirmation

if not working, remove the lines with key "eval.[..]" in .config/jetbrains/options/other.xml

'


