REPOSITORY="__REPOSITORY__"

if [ "$REPOSITORY" = "__REPOSITORY__" ]; then
    echo "Please, replace variable REPOSITORY by your repository"
    exit
fi

update-server()
{
    echo "Updating server"
    apt-get update && apt-get upgrade -y

}

update-app()
{
    git clone $REPOSITORY /fixed_app/tmp

    if [ ! -d "/fixed_app/backup" ]
    then
        mkdir /fixed_app/backup
    fi

    read -p "Purge backup ? [y/n] " response

    if [ "$response" = 'y' ]
    then
        echo "Suppress backup"
        rm -rf /fixed_app/backup/*
    else
        rm -rf /fixed_app/tmp
        echo "exiting..."
        exit
    fi

    echo 'Creation of backup ...'
    cp -rf /fixed_app/target_inst/* /fixed_app/backup

    echo 'Updating application...'
    rm -rf /fixed_app/target_inst/templates && cp -rf /fixed_app/tmp/ /fixed_app/target_inst/
    rm -rf /fixed_app/target_inst/src && cp -rf /fixed_app/tmp/ /fixed_app/target_inst/
    rm -rf /fixed_app/target_inst/vendor && cp -rf /fixed_app/tmp/ /fixed_app/target_inst/

    rm -rf /fixed_app/tmp

    echo "Updating success"
}

--help()
{
    echo -e "
     ______________________________________________________________
    |                       CONTEXT                                |
    |______________________________________________________________|
    |   [update-server] : update this server                       |
    |   [update-app]    : update this application (with backup)    |
    |_____________________________________________________________ |

    [exit] : Quit
    "
}

listen()
{
    stop='null'

    while [ $stop = 'null' ]
    do
        echo ""
        echo -e "\e[1;33m___________________________________________________________________________________________________________\e[0m
\e[32mwebmaster$
        "
        read -p "->  " response

        if [[ "$response" != "exit" && "$response" != "" ]]
        then

            $response

        elif [ "$response" = "exit" ]
        then

            echo -e '\e[46m\e[1;33mCome back when you want ! \e[0m\e[0m'
            exit
        fi
    done
}


listen
